package bitmap.java;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class BitMap {
    
    private int tamanho;
    private int bitmap;
    
    public BitMap(int tamanho) {
        this.tamanho = tamanho;
        this.bitmap = 0;
    }
    
    public void setBit(int posicao, int bit) {
        int mask = 0x01 << posicao;
        if(bit == 0) {
            this.bitmap = this.bitmap & ~mask;
        }
        else {
            this.bitmap = this.bitmap | mask;
        }
    }
    
    public void setBits(String bits) {
        int mask = 0x01;
        int iBitMap = 0;
        for (int iStr = this.tamanho-1; iStr >= 0; iStr--) {
            this.setBit(iBitMap, Character.getNumericValue(bits.charAt(iStr)));
            iBitMap ++;
        }
    }
        
    public String getBinario() {
        int mask = 0x01;
        String retorno = "";
        for (int i = 0; i < this.tamanho; i++) {
            retorno = (((this.bitmap & mask) == 0) ? "0" : "1") + retorno;
            mask = mask << 1;
        }
        return retorno;
    }
    
    public int getInteiro() {
        return this.bitmap;
    }
    
    public byte getByte() {
        return (byte) this.bitmap;
    }
    
    public String getHexadecimal() {
        return Integer.toHexString(bitmap);
    }
    
    public String toString() {
        return this.getBinario();
    }
    
    public static void main(String[] args) {
        BitMap bmTest = new BitMap(8);
        
        bmTest.setBit(0, 0);
        bmTest.setBit(1, 1);
        bmTest.setBit(2, 0);
        bmTest.setBit(3, 1);
        
        System.out.println(bmTest.getBinario());
        System.out.println(bmTest.getInteiro());        
        System.out.println(bmTest.getByte());
        System.out.println(bmTest.getHexadecimal());
        
        BitMap[] bma = new BitMap[4];
        bma[0] = new BitMap(8);
        bma[1] = new BitMap(8);
        bma[2] = new BitMap(8);
        bma[3] = new BitMap(8);
        
        bma[0].setBits("10101010");
        bma[1].setBits("00001111");
        bma[2].setBits("10000111");
        bma[3].setBits("11001100");
        
        System.out.println(Arrays.toString(bma));
        
        
        try {
            FileWriter fw = new FileWriter("memoria.txt");
            fw.write("v2.0 raw\n");
            for (BitMap bm : bma) {
                fw.write(bm.getHexadecimal() + " ");
            }
            fw.write("\n");
            fw.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        
        
        
    }
    
}
